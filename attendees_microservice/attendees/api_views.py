from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO
from django.views.decorators.http import require_http_methods
import json
import pika


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        "name",
        "import_href",
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(attendees, encoder=AttendeeListEncoder, safe=False)

    else:
        content = json.loads(request.body)

        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",

    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        if len(AccountVO.objects.filter(email=o.email)) > 0:
            return {
                "has_account": True,
            }
        else:
            return {
                "has_account": False,
            }


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse(
            {"message": "Attendee does not exist"},
            status=400,
        )
    if request.method == "GET":
        return JsonResponse(
            {"attendee": attendee}, encoder=AttendeeDetailEncoder, safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Attendee.objects.filter(id=id).update(**content)
        return JsonResponse(
            {"Attendee": attendee},
            encoder=AttendeeDetailEncoder,
            safe=False
        )
    else:
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


def pika_connect(content, queue):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(
        exchange="",
        routing_key=queue,
        body=json.dumps(content),
    )
    connection.close()
