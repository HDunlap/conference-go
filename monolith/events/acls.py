import requests
from .keys import PEXEL_KEY, OPENWEATHER_KEY
import json


def get_photo(city, state):
    headers = {"Authorization": PEXEL_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["url"]} #["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "appid": OPENWEATHER_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    resp = requests.get(url, params=params)
    content = resp.json()
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPENWEATHER_KEY
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    resp = requests.get(url, params=params)
    json_resp = resp.json()
    return json_resp
